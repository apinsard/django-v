# -*- coding: utf-8 -*-
from django.db.models.enums import ChoicesMeta, TextChoices

__all__ = [
    'ColoredTextChoices',
    'IconedColoredTextChoices',
    'IconedTextChoices',
]


class IconedChoicesMeta(ChoicesMeta):
    """A metaclass for creating a enum choices with icons."""

    def __new__(metacls, classname, bases, classdict):
        icons = []
        for key in classdict._member_names:
            value = classdict[key]
            *value, icon = value
            value = tuple(value)
            icons.append(icon)
            dict.__setitem__(classdict, key, value)
        cls = super().__new__(metacls, classname, bases, classdict)
        cls._value2icon_map_ = dict(zip(cls._value2member_map_, icons))
        cls.icon = property(lambda self: cls._value2icon_map_.get(self.value))
        return cls

    @property
    def choices(cls):
        empty = [(None, cls.__empty__[0])] if hasattr(cls, '__empty__') else []
        return empty + [(member.value, member.label) for member in cls]

    @property
    def icons(cls):
        empty = [cls.__empty__[1]] if hasattr(cls, '__empty__') else []
        return empty + [member.icon for member in cls]


class ColoredChoicesMeta(ChoicesMeta):
    """A metaclass for creating a enum choices with colors."""

    def __new__(metacls, classname, bases, classdict):
        colors = []
        for key in classdict._member_names:
            value = classdict[key]
            *value, color = value
            value = tuple(value)
            colors.append(color)
            dict.__setitem__(classdict, key, value)
        cls = super().__new__(metacls, classname, bases, classdict)
        cls._value2color_map_ = dict(zip(cls._value2member_map_, colors))
        cls.color = property(
            lambda self: cls._value2color_map_.get(self.value))
        return cls

    @property
    def choices(cls):
        empty = [(None, cls.__empty__[0])] if hasattr(cls, '__empty__') else []
        return empty + [(member.value, member.label) for member in cls]

    @property
    def colors(cls):
        empty = [cls.__empty__[1]] if hasattr(cls, '__empty__') else []
        return empty + [member.color for member in cls]


class IconedColoredChoicesMeta(ColoredChoicesMeta, IconedChoicesMeta):
    """A metaclass for creating a enum choices with icons and colors."""

    @property
    def colors(cls):
        empty = [cls.__empty__[2]] if hasattr(cls, '__empty__') else []
        return empty + [member.color for member in cls]


class IconedTextChoices(TextChoices, metaclass=IconedChoicesMeta):
    pass


class ColoredTextChoices(TextChoices, metaclass=ColoredChoicesMeta):
    pass


class IconedColoredTextChoices(TextChoices,
                               metaclass=IconedColoredChoicesMeta):
    pass
