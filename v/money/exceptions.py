# -*- coding: utf-8 -*-
__all__ = [
    'CurrencyMismatchError',
]


class CurrencyMismatchError(ValueError):
    pass
