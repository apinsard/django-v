# -*- coding: utf-8 -*-
from django.db import models

from v.money.models.fields import MoneyField


class Reservation(models.Model):

    total_price = MoneyField(null=True)
    total_paid = MoneyField(null=True)


class Stay(models.Model):

    reservation = models.ForeignKey(Reservation, models.PROTECT, related_name='stays')
    total_price = MoneyField(null=True)


class StayPriceLine(models.Model):

    stay = models.ForeignKey(Stay, models.CASCADE, related_name='price_lines')
    category = models.CharField(max_length=20)
    price = MoneyField()
