# -*- coding: utf-8 -*-
from decimal import Decimal

from django.test import TestCase

from v.money.models import Currency
from v.money.types import Money

from .models import Reservation, Stay, StayPriceLine


class MoneyFieldTestCase(TestCase):

    def setUp(self):
        Currency.objects.bulk_create([
            Currency(
                code='EUR', 
                decimals=2,
                name="Euro",
                symbol='€',
                rate=1,
            ),
            Currency(
                code='USD',
                decimals=2,
                name="US Dollar",
                symbol='$',
                rate=1.192265,
            ),
            Currency(
                code='JPY',
                decimals=0,
                name="Japanese Yen",
                symbol='¥',
                rate=124.4987,
            ),
            Currency(
                code='BTC',
                decimals=9,
                name="Bitcoin",
                symbol='₿',
            ),
        ])

    def test_money_field(self):
        r1 = Reservation(
            total_price=Money('10.00', 'EUR'),
            total_paid=Money('2.00', 'EUR'))
        self.assertEquals(r1.total_price, Money(10, 'EUR'))
        r1.save()
        r2 = Reservation.objects.get(pk=r1.pk)
        self.assertEquals(r2.total_price, Money(10, 'EUR'))
        r2._total_price_currency_id = 'USD'
        self.assertEquals(r2._total_price_currency, Currency['USD'])
        self.assertEquals(r2.total_price, Money(10, 'USD'))
        r2._total_price_currency = Currency['JPY']
        self.assertEquals(r2._total_price_currency_id, 'JPY')
        self.assertEquals(r2.total_price, Money(1000, 'JPY'))

    def test_money_amount_lookup(self):
        r1 = Reservation.objects.create(
            total_price=Money(10, 'EUR'),
            total_paid=Money(2, 'EUR'))
        r2 = Reservation.objects.create(
            total_price=Money(10, 'USD'),
            total_paid=Money(2, 'USD'))
        r3 = Reservation.objects.create(
            total_price=Money(5, 'EUR'),
            total_paid=Money(2, 'EUR'))
        reservations = (
            Reservation.objects
            .filter(total_price__amount=10)
            .values_list('pk', flat=True)
        )
        print(repr(reservations), type(reservations), list(reservations))
        self.assertIn(r1.pk, reservations)
        self.assertIn(r2.pk, reservations)
        self.assertNotIn(r3.pk, reservations)

    def test_money_currency_lookup(self):
        r1 = Reservation.objects.create(
            total_price=Money(10, 'EUR'),
            total_paid=Money(2, 'EUR'))
        r2 = Reservation.objects.create(
            total_price=Money(10, 'USD'),
            total_paid=Money(2, 'USD'))
        r3 = Reservation.objects.create(
            total_price=Money(5, 'EUR'),
            total_paid=Money(2, 'EUR'))
        s1 = Stay.objects.create(
            reservation=r1, total_price=Money(5, 'EUR'))
        stays = (
            Stay.objects.filter(reservation__total_price__currency='EUR')
        )
        reservations = (
            Reservation.objects
            .filter(total_price__currency='EUR')
            .values_list('pk', flat=True)
        )
        self.assertIn(r1.pk, reservations)
        self.assertNotIn(r2.pk, reservations)
        self.assertIn(r3.pk, reservations)
