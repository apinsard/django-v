# -*- coding: utf-8 -*-
from decimal import Decimal
import math

from django.test import TestCase
from v.money.exceptions import CurrencyMismatchError
from v.money.models import Currency
from v.money.types import Money


class MoneyTestCase(TestCase):

    def setUp(self):
        Currency.objects.bulk_create([
            Currency(
                code='EUR', 
                decimals=2,
                name="Euro",
                symbol='€',
                rate=1,
            ),
            Currency(
                code='USD',
                decimals=2,
                name="US Dollar",
                symbol='$',
                rate=1.192265,
            ),
            Currency(
                code='JPY',
                decimals=0,
                name="Japanese Yen",
                symbol='¥',
                rate=124.4987,
            ),
            Currency(
                code='BTC',
                decimals=9,
                name="Bitcoin",
                symbol='₿',
            ),
        ])

    def test_precision(self):
        money1 = Money('2.50', 'EUR')
        money2 = Money(2.5, 'USD', extra_precision=2)
        money3 = Money(300.1234, 'JPY', extra_precision=2)
        self.assertEquals(money1.precision, 2)
        self.assertEquals(money2.precision, 4)
        self.assertEquals(money3.precision, 2)

    def test_as_scu(self):
        money1 = Money('2.50', 'EUR')
        money2 = Money(2.5, 'USD', extra_precision=2)
        money3 = Money(300.1234, 'JPY', extra_precision=2)
        self.assertEquals(money1.as_scu(), Decimal('250'))
        self.assertEquals(money2.as_scu(), Decimal('250'))
        self.assertEquals(money3.as_scu(), Decimal('300.12'))

    def test_as_tuple(self):
        money1 = Money('2.50', 'EUR')
        money2 = Money('250', 'USD', extra_precision=2)
        self.assertEquals(money1.as_tuple(), (Decimal('2.50'), 'EUR'))
        self.assertEquals(money2.as_tuple(), (Decimal('250'), 'USD'))

    def test_pos(self):
        money1 = Money(1.234, 'EUR', extra_precision=1)
        self.assertEquals(+money1, money1)

    def test_neg(self):
        self.assertEquals(-Money(1, 'EUR'), Money(-1, 'EUR'))

    def test_add_money(self):
        money1 = Money('2.50', 'EUR')
        money2 = Money('1.25', 'EUR')
        money3 = Money('1.23', 'EUR', extra_precision=1)
        money4 = Money('1.265', 'EUR', extra_precision=1)
        money12 = Money('3.75', 'EUR')
        money13 = Money('3.73', 'EUR', extra_precision=1)
        money34 = Money('2.495', 'EUR', extra_precision=1)
        self.assertEquals(money1 + money2, money12)
        self.assertEquals(money1 + money3, money13)
        self.assertEquals(money3 + money4, money34)
        self.assertEquals(money12.extra_precision, 0)
        self.assertEquals(money13.extra_precision, 1)
        self.assertEquals(money34.extra_precision, 1)

    def test_add_decimal(self):
        money1 = Money('125.10', 'JPY', extra_precision=1)
        self.assertEquals(money1 + Decimal('4.96'), Money('130.1', 'JPY', extra_precision=1))

    def test_add_float(self):
        money1 = Money('125.10', 'USD')
        self.assertEquals(money1 + 4.9, Money('130', 'USD'))

    def test_add_str(self):
        money1 = Money('125.10', 'EUR')
        self.assertEquals(money1 + '.90', Money(126, 'EUR'))

    def test_add_cross_currency(self):
        money1 = Money(5, 'EUR')
        money2 = Money(5, 'USD')
        with self.assertRaises(ValueError):
            money1 + money2

    def test_add_notimplemented(self):
        money1 = Money('125.10', 'EUR')
        with self.assertRaises(TypeError):
            money1 + True

    def test_radd_decimal(self):
        money1 = Money('125.10', 'JPY', extra_precision=1)
        self.assertEquals(Decimal('4.96') + money1, Money('130.1', 'JPY', extra_precision=1))

    def test_radd_float(self):
        money1 = Money('125.10', 'USD')
        self.assertEquals(4.9 + money1, Money('130', 'USD'))

    def test_radd_str(self):
        money1 = Money('125.10', 'EUR')
        self.assertEquals('.90' + money1, Money(126, 'EUR'))

    def test_iadd_money(self):
        money1 = Money('125.125', 'EUR', extra_precision=1)
        money2 = Money('125.1234', 'EUR', extra_precision=2)
        money3 = money1
        money1 += money2
        self.assertEquals(money1, Money('250.2484', 'EUR', extra_precision=2))
        self.assertIsNot(money1, money3)

    def test_iadd_decimal(self):
        money1 = Money('125', 'JPY')
        money1 += Decimal('5')
        self.assertEquals(money1, Money('130', 'JPY'))

    def test_iadd_int(self):
        money1 = Money('125', 'JPY')
        money1 += 5
        self.assertEquals(money1, Money('130', 'JPY'))

    def test_iadd_str(self):
        money1 = Money('125', 'USD')
        money1 += '5.55'
        self.assertEquals(money1, Money('130.55', 'USD'))

    def test_iadd_cross_currency(self):
        money1 = Money(1, 'EUR')
        money2 = Money(1, 'USD')
        with self.assertRaises(ValueError):
            money1 += money2

    def test_iadd_notimplemented(self):
        money1 = Money('125.10', 'EUR')
        with self.assertRaises(TypeError):
            money1 += True

    def test_sub(self):
        money1 = Money(5, 'USD')
        money2 = Money(15, 'USD')
        self.assertEquals(money1 - money2, Money(-10, 'USD'))
        self.assertEquals(money1 - 15, Money(-10, 'USD'))
        self.assertEquals(money1 - '15', Money(-10, 'USD'))

    def test_rsub(self):
        money1 = Money('2.2', 'USD')
        self.assertEquals(5 - money1, Money(2.8, 'USD'))
        self.assertEquals(Decimal(5) - money1, Money(2.8, 'USD'))
        self.assertEquals('5' - money1, Money(2.8, 'USD'))

    def test_isub(self):
        money1 = Money(100, 'EUR')
        money1 -= Money(75, 'EUR')
        self.assertEquals(money1, Money(25, 'EUR'))
        money1 -= Decimal('6.66')
        self.assertEquals(money1, Money('18.34', 'EUR'))
        money1 -= 20
        self.assertEquals(money1, Money('-1.66', 'EUR'))
        money1 -= '-6.66'
        self.assertEquals(money1, Money(5, 'EUR'))

    def test_mul(self):
        money1 = Money(100000, 'JPY', extra_precision=2)
        self.assertEquals(money1 * 12, Money(1200000, 'JPY'))
        self.assertEquals(money1 * 12.3, Money(1230000, 'JPY'))
        self.assertEquals(money1 * Decimal('12.3456789'), Money('1234567.89', 'JPY', extra_precision=2))
        self.assertEquals(money1 * Decimal('1.23456789'), Money('123456.79', 'JPY', extra_precision=2))

    def test_mul_money(self):
        money1 = Money(1, 'JPY')
        money2 = Money(2, 'JPY')
        with self.assertRaises(TypeError):
            money1 * money2

    def test_rmul(self):
        money1 = Money(5, 'EUR')
        self.assertEquals(5 * money1, Money(25, 'EUR'))
        self.assertEquals(2.5 * money1, Money(12.5, 'EUR'))
        self.assertEquals(Decimal('0.5') * money1, Money('2.5', 'EUR'))

    def test_imul(self):
        money1 = Money(1000, 'EUR')
        money2 = money1
        money1 *= 5
        self.assertEquals(money1, Money(5000, 'EUR'))
        money1 *= .5
        self.assertEquals(money1, Money(2500, 'EUR'))
        money1 *= Decimal('1.234567')
        self.assertEquals(money1, Money('3086.42', 'EUR'))
        self.assertIsNot(money1, money2)

    def test_imul_money(self):
        money1 = Money(1, 'JPY')
        money2 = Money(2, 'JPY')
        with self.assertRaises(TypeError):
            money1 *= money2

    def test_truediv(self):
        money1 = Money(99, 'USD')
        self.assertEquals(money1 / 3, Money(33, 'USD'))
        self.assertEquals(money1 / 9.0, Money(11, 'USD'))
        self.assertEquals(money1 / Decimal('27.00'), Money('3.67', 'USD'))

    def test_truediv_money(self):
        money1 = Money(1, 'EUR')
        money2 = Money(2, 'EUR')
        with self.assertRaises(TypeError):
            money1 / money2

    def test_itruediv(self):
        money1 = Money(1000, 'EUR', extra_precision=7)
        money2 = money1
        money1 /= 5
        self.assertEquals(money1, Money(200, 'EUR'))
        money1 /= .5
        self.assertEquals(money1, Money(400, 'EUR'))
        money1 /= Decimal('1.234567')
        self.assertEquals(money1, Money('324.00023652', 'EUR', extra_precision=7))
        self.assertIsNot(money1, money2)

    def test_itruediv_money(self):
        money1 = Money(1, 'EUR')
        money2 = Money(2, 'EUR')
        with self.assertRaises(TypeError):
            money1 /= money2

    def test_rshift_currency(self):
        money1 = Money(10, 'EUR')
        self.assertEquals(money1 >> Currency['USD'], Money('11.92', 'USD'))
        self.assertEquals(money1 >> 'USD', Money('11.92', 'USD'))

    def test_rshift_unrated(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(ValueError):
            money1 >> 'EUR'
        with self.assertRaises(ValueError):
            money2 >> Currency['BTC']
        with self.assertRaises(ValueError):
            money1 >> money2

    def test_rshift_money(self):
        money1 = Money(10, 'EUR')
        money2 = Money(10, 'USD')
        self.assertEquals(money1 >> money2, Money('21.92', 'USD'))

    def test_rshift_amount(self):
        money1 = Money(10, 'EUR')
        self.assertEquals(money1 >> 10, Money('20', 'EUR'))
        self.assertEquals(money1 >> 10.5, Money('20.50', 'EUR'))
        self.assertEquals(money1 >> Decimal('10.05'), Money('20.05', 'EUR'))

    def test_irshift_currency(self):
        money1 = Money(10, 'EUR')
        money2 = money1
        money1 >>= 'USD'
        self.assertEquals(money1, Money('11.92', 'USD'))
        money1 >>= Currency['EUR']
        self.assertEquals(money1, Money('10.00', 'EUR'))
        self.assertIsNot(money1, money2)

    def test_irshift_unrated(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(ValueError):
            money1 >>= 'EUR'
        with self.assertRaises(ValueError):
            money2 >>= Currency['BTC']

    def test_irshift_money(self):
        money1 = Money(10, 'EUR')
        money1 >>= Money(10, 'USD')
        self.assertEquals(money1, Money('21.92', 'USD'))

    def test_irshift_amount(self):
        money1 = Money(10, 'EUR')
        money1 >>= 10
        self.assertEquals(money1, Money('20', 'EUR'))
        money1 >>= 10.5
        self.assertEquals(money1, Money('30.50', 'EUR'))
        money1 >>= Decimal('10.05')
        self.assertEquals(money1, Money('40.55', 'EUR'))

    def test_lshift_currency(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(TypeError):
            money1 << 'EUR'
        with self.assertRaises(TypeError):
            money2 << Currency['BTC']

    def test_lshift_unrated(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(ValueError):
            money1 << money2

    def test_lshift_money(self):
        money1 = Money(10, 'EUR')
        money2 = Money(10, 'USD')
        self.assertEquals(money1 << money2, Money('18.39', 'EUR'))

    def test_lshift_amount(self):
        money1 = Money(10, 'EUR')
        self.assertEquals(money1 << 10, Money('20', 'EUR'))
        self.assertEquals(money1 << 10.5, Money('20.50', 'EUR'))
        self.assertEquals(money1 << Decimal('10.05'), Money('20.05', 'EUR'))

    def test_ilshift_currency(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(TypeError):
            money1 <<= 'EUR'
        with self.assertRaises(TypeError):
            money2 <<= Currency['BTC']

    def test_ilshift_unrated(self):
        money1 = Money(1, 'BTC')
        money2 = Money(20000, 'USD')
        with self.assertRaises(ValueError):
            money1 <<= money2

    def test_ilshift_money(self):
        money1 = Money(10, 'EUR')
        money2 = money1
        money1 <<= Money(10, 'USD')
        self.assertEquals(money1, Money('18.39', 'EUR'))
        self.assertIsNot(money1, money2)

    def test_ilshift_amount(self):
        money1 = Money(10, 'EUR')
        money1 <<= 10
        self.assertEquals(money1, Money('20', 'EUR'))
        money1 <<= 10.5
        self.assertEquals(money1, Money('30.50', 'EUR'))
        money1 <<= Decimal('10.05')
        self.assertEquals(money1, Money('40.55', 'EUR'))

    def test_abs(self):
        money1 = Money(10, 'EUR')
        money2 = Money('-500.93', 'EUR')
        self.assertEquals(abs(money1), Money('10', 'EUR'))
        self.assertEquals(abs(money2), Money('500.93', 'EUR'))

    def test_round(self):
        money1 = Money('10.50', 'EUR')
        money2 = Money('-500.93', 'EUR')
        self.assertEquals(round(money1, 1), Money('10.50', 'EUR'))
        self.assertEquals(round(money2), Money('-501.00', 'EUR'))

    def test_trunc(self):
        money1 = Money('10.50', 'EUR')
        money2 = Money('-500.93', 'EUR')
        self.assertEquals(math.trunc(money1), Money('10.00', 'EUR'))
        self.assertEquals(math.trunc(money2), Money('-500.00', 'EUR'))

    def test_floor(self):
        money1 = Money('10.50', 'EUR')
        money2 = Money('-500.93', 'EUR')
        self.assertEquals(math.floor(money1), Money('10.00', 'EUR'))
        self.assertEquals(math.floor(money2), Money('-501.00', 'EUR'))

    def test_ceil(self):
        money1 = Money('10.50', 'EUR')
        money2 = Money('-500.93', 'EUR')
        self.assertEquals(math.ceil(money1), Money('11.00', 'EUR'))
        self.assertEquals(math.ceil(money2), Money('-500.00', 'EUR'))

    def test_bool(self):
        money1 = Money('0.5', 'USD')
        money2 = Money(0, 'USD')
        money3 = Money(-500, 'USD')
        self.assertTrue(bool(money1))
        self.assertFalse(bool(money2))
        self.assertTrue(bool(money3))

    def test_float(self):
        money1 = Money('123.45', 'USD')
        self.assertEquals(float(money1), 123.45)

    def test_int(self):
        money1 = Money('123.54', 'USD')
        self.assertEquals(int(money1), 12354)

    def test_hash(self):
        moneys = {
            Money(1, 'EUR'), Money('1.00', 'EUR', extra_precision=4),
            Money('1.01', 'EUR'), Money('1.011', 'EUR'),
            Money('1.011', 'EUR', extra_precision=1),
            Money(0, 'USD'), Money('0.00', 'USD'),
            Money('1', 'USD'), Money(1., 'USD', extra_precision=2),
        }
        self.assertEquals(len(moneys), 5)

    def test_eq(self):
        self.assertTrue(Money(10, 'USD') == Money(10, 'USD', extra_precision=2))
        self.assertFalse(Money(10, 'USD') == Money(10, 'EUR'))
        self.assertTrue(Money(10, 'USD') == 10)
        self.assertTrue(Money(10, 'EUR') == 10)
        self.assertTrue(Money(10, 'EUR') == 10 == Money(10, 'USD'))   # This is a shame
        self.assertFalse(10 == Money(10, 'EUR') == Money(10, 'USD'))  # But this is a fact
        self.assertTrue(Money(10, 'USD') == Money(8.39, 'EUR'))
        self.assertFalse(Money(10, 'USD') == 8.39)
        self.assertFalse(Money(10, 'USD') == 'USD')
        self.assertTrue(Money(10, 'BTC') == Money(10, 'BTC'))
        with self.assertRaises(CurrencyMismatchError):
            Money(10, 'BTC') == Money(10, 'EUR')

    def test_ne(self):
        self.assertFalse(Money(10, 'USD') != Money(10, 'USD', extra_precision=2))
        self.assertTrue(Money(10, 'USD') != Money(10, 'EUR'))
        self.assertFalse(Money(10, 'USD') != 10)
        self.assertFalse(Money(10, 'EUR') != 10)
        self.assertFalse(Money(10, 'USD') != Money(8.39, 'EUR'))
        self.assertTrue(Money(10, 'USD') != 8.39)
        self.assertTrue(Money(10, 'USD') != 'USD')
        self.assertFalse(Money(10, 'BTC') != Money(10, 'BTC'))
        with self.assertRaises(CurrencyMismatchError):
            Money(10, 'BTC') != Money(10, 'EUR')

    def test_lt(self):
        self.assertTrue(Money(10, 'USD') < Money(11, 'USD'))
        self.assertFalse(Money(10, 'USD') < Money(10, 'USD'))
        self.assertFalse(Money(10, 'USD') < Money(9, 'USD'))
        self.assertFalse(Money(10, 'USD') < Money(8.39, 'EUR'))
        self.assertTrue(Money(10, 'USD') < Money(8.40, 'EUR'))
        self.assertFalse(Money(10, 'USD') < Money(8.38, 'EUR'))
        self.assertTrue(Money(10, 'USD') < 10.01)
        self.assertFalse(Money(10, 'USD') < 9.99)

    def test_gt(self):
        self.assertFalse(Money(10, 'USD') > Money(11, 'USD'))
        self.assertFalse(Money(10, 'USD') > Money(10, 'USD'))
        self.assertTrue(Money(10, 'USD') > Money(9, 'USD'))
        self.assertFalse(Money(10, 'USD') > Money(8.39, 'EUR'))
        self.assertFalse(Money(10, 'USD') > Money(8.40, 'EUR'))
        self.assertTrue(Money(10, 'USD') > Money(8.38, 'EUR'))
        self.assertFalse(Money(10, 'USD') > 10.01)
        self.assertTrue(Money(10, 'USD') > 9.99)

    def test_le(self):
        self.assertTrue(Money(10, 'USD') <= Money(11, 'USD'))
        self.assertTrue(Money(10, 'USD') <= Money(10, 'USD'))
        self.assertFalse(Money(10, 'USD') <= Money(9, 'USD'))
        self.assertTrue(Money(10, 'USD') <= Money(8.39, 'EUR'))
        self.assertTrue(Money(10, 'USD') <= Money(8.40, 'EUR'))
        self.assertFalse(Money(10, 'USD') <= Money(8.38, 'EUR'))
        self.assertTrue(Money(10, 'USD') <= 10.01)
        self.assertFalse(Money(10, 'USD') <= 9.99)

    def test_ge(self):
        self.assertFalse(Money(10, 'USD') >= Money(11, 'USD'))
        self.assertTrue(Money(10, 'USD') >= Money(10, 'USD'))
        self.assertTrue(Money(10, 'USD') >= Money(9, 'USD'))
        self.assertTrue(Money(10, 'USD') >= Money(8.39, 'EUR'))
        self.assertFalse(Money(10, 'USD') >= Money(8.40, 'EUR'))
        self.assertTrue(Money(10, 'USD') >= Money(8.38, 'EUR'))
        self.assertFalse(Money(10, 'USD') >= 10.01)
        self.assertTrue(Money(10, 'USD') >= 9.99)
