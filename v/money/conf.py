# -*- coding: utf-8 -*-
from django.core.exceptions import ImproperlyConfigured
from django.conf import settings

__all__ = [
    'DEFAULT_CURRENCY',
    'CURRENCY_CACHE_TIMEOUT',
]

try:
    DEFAULT_CURRENCY = settings.VMONEY['DEFAULT_CURRENCY']
except (AttributeError, KeyError):
    raise ImproperlyConfigured(
        "Please define VMONEY['DEFAULT_CURRENCY'] in your settings.")

CURRENCY_CACHE_TIMEOUT = settings.VMONEY.get('CURRENCY_CACHE_TIMEOUT', 3600)
