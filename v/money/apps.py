from django.apps import AppConfig


class MoneyConfig(AppConfig):
    name = 'v.money'
