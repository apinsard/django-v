# -*- coding: utf-8 -*-
MONEY_POSITIVE_FORMAT = MONEY_ZERO_FORMAT = '{amount} {symbol}'
MONEY_NEGATIVE_FORMAT = '-{amount} {symbol}'
