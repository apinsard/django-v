# -*- coding: utf-8 -*-
MONEY_POSITIVE_FORMAT = MONEY_ZERO_FORMAT = '{symbol}{amount}'
MONEY_NEGATIVE_FORMAT = '-{symbol}{amount}'
