# -*- coding: utf-8 -*-
from django.db.models import Transform, F, ExpressionWrapper, DecimalField
from django.db.models.expressions import Col

__all__ = [
    'AmountTransform', 'CurrencyTransform',
]


class AmountTransform(Transform):
    """
    'money' / 10^('currency'.'decimals') = value
    OU
    'money' = value * 10^('currency'.'decimals')
    """
    lookup_name = 'amount'
    template = '%(expressions)s'
    arity = 1

    def __init__(self, lhs):
        # currency_field = lhs.target.currency_field
        # print(currency_field.name)
        # lhs = lhs / 10**F(f'{currency_field.name}__decimals')
        # print(lhs)
        super().__init__(lhs)

    def as_sql(self, compiler, connection):
        from django.db.models.sql.where import WhereNode, AND
        # print(self.lhs.target, self.lhs.source)
        # print(self.lhs.target.get_col(self.lhs.alias, self.lhs.source))
        root_constraint = WhereNode()
        currency_field = self.lhs.target.currency_field.name
        root_constraint.add(
            ExpressionWrapper(
                self.lhs / 10**F(f'{currency_field}__decimals'),
                output_field=DecimalField()),
            AND)
        return root_constraint.as_sql(compiler, connection)


class CurrencyTransform(Transform):
    """
    'currency' = value
    """
    lookup_name = 'currency'
    template = '%(expressions)s'

    def __init__(self, lhs):
        if not isinstance(lhs, Col):
            raise TypeError("'lhs' argument must be a Col. Not %r." % repr(lhs))
        lhs = lhs.target.currency_field.get_col(lhs.alias)
        super().__init__(lhs)
