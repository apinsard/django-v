# -*- coding: utf-8 -*-
from django.db import models, router
from django.db.models.expressions import BaseExpression
from django.db.models.fields.related_descriptors import (
    ForeignKeyDeferredAttribute, ForwardManyToOneDescriptor,
)
from django.db.models.query_utils import DeferredAttribute

from .base import Currency
from .lookups import AmountTransform, CurrencyTransform
from ..types import Money

__all__ = [
    'CurrencyField', 'MoneyField',
]


class CurrencyKeyDescriptor(ForeignKeyDeferredAttribute):

    def __set__(self, instance, value):
        if value is None:
            currency = None
        elif isinstance(value, str):
            currency = Currency[value]
        else:
            raise ValueError(f"'{value!r}' is not a valid Currency code.")
        instance.__dict__[self.field.name] = currency
        instance.__dict__[self.field.attname] = value


class CurrencyDescriptor(ForwardManyToOneDescriptor):

    def __get__(self, instance, cls=None):
        if instance is None:
            return self
        return instance.__dict__[self.field.name]

    def __set__(self, instance, value):
        if value is None:
            key = None
        elif isinstance(value, Currency):
            key = value.code
        elif isinstance(value, str):
            key = value
            value = Currency[key]
        else:
            raise ValueError(
                'Cannot assign "%r": "%s.%s" must be a "%s" instance.' % (
                    value,
                    instance._meta.object_name,
                    self.field.name,
                    Currency._meta.object_name,
                )
            )
        if value is not None:
            # Block copy-pasted from ForwardManyToOneDescriptor. Not really sure what it does.
            # Could possibly be removed in the future if we can proove it useless.
            if instance._state.db is None:
                instance._state.db = router.db_for_write(instance.__class__, instance=value)
            if value._state.db is None:
                value._state.db = router.db_for_write(value.__class__, instance=instance)
            if not router.allow_relation(value, instance):
                raise ValueError((
                    'Cannot assign "%r": the current database router prevents '
                    'this relation.'
                ) % value)
        instance.__dict__[self.field.name] = value
        instance.__dict__[self.field.attname] = key


class CurrencyField(models.ForeignKey):

    description = "A currency"
    descriptor_class = CurrencyKeyDescriptor
    forward_related_accessor_class = CurrencyDescriptor

    def __init__(self, *, price_field=None, choices=None, **kwargs):
        self.price_field = price_field
        self.choices = choices
        kwargs.setdefault('related_name', '+')
        if self.choices:
            kwargs['limit_choices_to'] = {'code__in': choices}
        kwargs['to'] = 'money.Currency'
        kwargs['on_delete'] = models.PROTECT
        super().__init__(**kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        kwargs['price_field'] = self.price_field
        if self.choices:
            kwargs['choices'] = self.choices
            del kwargs['limit_choices_to']
        return name, path, args, kwargs

    def contribute_to_class(self, cls, name):
        # For some obscure reason, this method seems to be executed twice.
        # This tweak prevents the creation of a duplicated db column.
        # This would benefit from being clarified and hopefully find a cleaner solution.
        if name not in [f.name for f in cls._meta.fields]:
            super().contribute_to_class(cls, name)


class MoneyDescriptor(DeferredAttribute):

    def __get__(self, instance, cls=None):
        if instance is None:
            return self
        value = super().__get__(instance, cls)
        if value is None or isinstance(value, (BaseExpression, Money)):
            return value
        currency = self._get_currency(instance)
        value /= 10**currency.decimals
        extra_precision = self.field.decimal_places
        return Money(value, currency, extra_precision=extra_precision)

    def __set__(self, instance, value):
        currency = None
        if isinstance(value, Money):
            currency = value.currency
            value = value.as_scu()
        elif isinstance(value, BaseExpression):
            raise NotImplementedError  # TODO Implement expressions
        instance.__dict__[self.field.name] = value
        if value is None:
            setattr(instance, self.field.get_currency_field_name(), None)
        elif currency is not None:
            setattr(instance, self.field.get_currency_field_name(), currency)

    def _get_currency(self, instance):
        currency = instance.__dict__[self.field.get_currency_field_name()]
        return currency


class MoneyField(models.DecimalField):

    descriptor_class = MoneyDescriptor
    description = "A monetary amount"

    def __init__(self, *, max_digits=12, extra_precision=0,
                 currency_field_name=None, currency_choices=None,
                 **kwargs):
        kwargs['max_digits'] = max_digits + extra_precision
        kwargs['decimal_places'] = extra_precision
        self.currency_field_name = currency_field_name
        self.currency_choices = currency_choices
        super().__init__(**kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        kwargs['extra_precision'] = kwargs['decimal_places']
        kwargs['max_digits'] -= kwargs['extra_precision']
        del kwargs['decimal_places']
        kwargs['currency_field_name'] = self.currency_field_name
        kwargs['currency_choices'] = self.currency_choices
        return name, path, args, kwargs

    def contribute_to_class(self, cls, name):
        if not hasattr(self, 'currency_field'):
            self._add_currency_field(cls, name)
        super().contribute_to_class(cls, name)

    def get_currency_field_name(self, name=None):
        name = name or self.name
        return self.currency_field_name or f'_{name}_currency'

    def _add_currency_field(self, cls, name):
        currency_field_name = self.get_currency_field_name(name)
        currency_field = getattr(cls, currency_field_name, None)
        if not currency_field:
            currency_field = CurrencyField(
                price_field=self, choices=self.currency_choices,
                null=self.null, auto_created=True)
            cls.add_to_class(currency_field_name, currency_field)
        self.currency_field = currency_field

    def to_python(self, value):
        if isinstance(value, Money):
            value = value.as_scu()
        return super().to_python(value)

    def get_prep_value(self, value):
        if isinstance(value, Money):
            value = value.as_scu()
        return super().get_prep_value(value)


MoneyField.register_lookup(AmountTransform)
MoneyField.register_lookup(CurrencyTransform)
