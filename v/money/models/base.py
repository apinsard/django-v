# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from django.core.validators import RegexValidator, MinValueValidator
from django.db import models
from django.db.models.base import ModelBase
from django.utils.translation import gettext_lazy as _

from ..conf import DEFAULT_CURRENCY, CURRENCY_CACHE_TIMEOUT

__all__ = [
    'Currency',
]


class CurrencyMetaclass(ModelBase):

    _cache = {}
    _cache_expiry = None

    @property
    def default(cls):
        return cls[DEFAULT_CURRENCY]

    def refresh_cache(cls):
        old_keys = set(cls._cache.keys())
        for currency in cls.objects.all():
            cls._cache[currency.code] = currency
            try:
                old_keys.remove(currency.code)
            except KeyError:
                pass
        cls._cache_expiry = datetime.now() + timedelta(seconds=CURRENCY_CACHE_TIMEOUT)
        # Remove old keys only after cache was updated to avoid empty cache
        for key in old_keys:
            del cls._cache[key]

    def __getitem__(cls, key):
        if cls._cache_expired() or key not in cls._cache:
            cls.refresh_cache()
        return cls._cache[key]

    def __iter__(cls):
        if cls._cache_expired():
            cls.refresh_cache()
        return iter(cls._cache.values())

    def _cache_expired(cls):
        return not cls._cache_expiry or cls._cache_expiry < datetime.now()


class Currency(models.Model, metaclass=CurrencyMetaclass):

    class Meta:
        verbose_name = _("currency")
        verbose_name_plural = _("currencies")

    code = models.CharField(
        primary_key=True, max_length=6,
        verbose_name=_("code"),
        validators=[
            RegexValidator(r'^[A-Z0-9]+$', _(
                "The currency code can only include uppercase letters and digits."
            )),
            RegexValidator(r'[A-Z]', _(
                "The currency code must include at least one letter."
            )),
        ])
    decimals = models.PositiveSmallIntegerField(
        verbose_name=_("number of decimals"))
    name = models.CharField(
        max_length=6,
        verbose_name=_("name"))
    symbol = models.CharField(
        max_length=6,
        verbose_name=_("symbol"))
    rate = models.FloatField(
        blank=True, null=True,
        verbose_name=_("conversion rate"),
        help_text=_("Relative to default currency: 1 THIS = <rate> DEFAULT."),
        validators=[
            MinValueValidator(0, _("Currency rate cannot be negative.")),
        ])

    def __str__(self):
        return self.code

    def is_default(self):
        return self.code == DEFAULT_CURRENCY
