# -*- coding: utf-8 -*-
import decimal
import math
from decimal import Decimal

from django.utils.formats import get_format

from .conf import DEFAULT_CURRENCY
from .models import Currency
from .exceptions import CurrencyMismatchError

__all__ = [
    'Money',
]


class Money:

    def __init__(self, amount=0, currency=None, *, extra_precision=0):
        self.currency = currency or DEFAULT_CURRENCY
        self.extra_precision = extra_precision
        self.amount = amount

    @property
    def amount(self):
        return self._amount

    @amount.setter
    def amount(self, value):
        self._amount = force_decimal(value, self.precision)

    @property
    def currency(self):
        return self._currency

    @currency.setter
    def currency(self, value):
        if not isinstance(value, Currency):
            value = Currency[value]
        self._currency = value

    @property
    def precision(self):
        return self.currency.decimals + self.extra_precision

    def as_scu(self):
        """Return the amount as Smallest Currency Unit."""
        return round(self.amount * 10**self.currency.decimals, self.extra_precision)

    def as_tuple(self):
        return (self.amount, self.currency.code)

    def format(self, group_size=None, group_separator=None, decimal_separator=None,
               positive_format=None, negative_format=None, zero_format=None):
        group_size = group_size or get_format('NUMBER_GROUPING')
        group_separator = group_separator or get_format('THOUSAND_SEPARATOR')
        decimal_separator = decimal_separator or get_format('DECIMAL_SEPARATOR')

        amount = abs(self.amount)
        integral = int(math.trunc(amount))
        if group_size:
            parts = []
            while integral > 0:
                integral, part = divmod(integral, 10**group_size)
                parts.insert(0, str(part).zfill(group_size))
            integral = group_separator.join(parts).lstrip('0')
        else:
            integral = str(integral)
        p = self.precision
        fractional = str(int(amount * 10**p % p)).zfill(p)
        amount = decimal_separator.join((integral, fractional))

        if self.amount > 0:
            fmt = positive_format or self._get_format('POSITIVE')
        elif self.amount < 0:
            fmt = negative_format or self._get_format('NEGATIVE')
        else:
            fmt = zero_format or self._get_format('ZERO')

        return fmt.format(
            amount=amount,
            symbol=self.currency.symbol,
            code=self.currency.code,
            name=self.currency.name,
        )

    def __str__(self):
        return self.format()

    def __repr__(self):
        return f"<Money: {self.currency} {self.amount}>"

    def __pos__(self):
        return Money(self.amount, self.currency, **self._options)

    def __neg__(self):
        return Money(-self.amount, self.currency, **self._options)

    def __add__(self, other):
        if isinstance(other, Money):
            extra_precision = max(self.extra_precision, other.extra_precision)
            if self.currency == other.currency:
                other = other.amount
            else:
                raise CurrencyMismatchError(
                    "Cross-currency operations not allowed. "
                    "Use << operator to add currencies with conversion rate.")
        else:
            extra_precision = self.extra_precision
            try:
                other = force_decimal(other)
            except ValueError:
                return NotImplemented
        amount = self.amount + other
        return Money(amount, self.currency, extra_precision=extra_precision)

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        if not isinstance(other, Money):
            try:
                other = force_decimal(other)
            except ValueError:
                return NotImplemented
        return self.__add__(-other)

    def __rsub__(self, other):
        return (-self).__add__(other)

    def __mul__(self, other):
        if isinstance(other, Money):
            raise TypeError("Cannot multiply currencies together.")
        try:
            other = force_decimal(other)
        except ValueError:
            return NotImplemented
        amount = self.amount * other
        return Money(amount, self.currency, **self._options)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        if isinstance(other, Money):
            raise TypeError("Cannot divide currencies together.")
        try:
            other = force_decimal(other)
        except ValueError:
            return NotImplemented
        amount = self.amount / other
        return Money(amount, self.currency, **self._options)

    def __rshift__(self, other):
        """Currency conversion.

        For instance:
            Money(10, 'EUR') >> 'USD'

        Will convert 10 EUR to USD.

        Can also be used to do cross-currency sums:
            Money(10, 'EUR') >> Money(5, 'USD')

        Will first convert 10 EUR to USD, then add 5 USD to the result and
        return the total in USD.

        This requires both currencies rate to be defined.
        """
        currency = self.currency
        extra_precision = self.extra_precision
        if isinstance(other, Money):
            currency = other.currency
            extra_precision = max(extra_precision, other.extra_precision)
            other = other.amount
        elif isinstance(other, Currency):
            currency = other
            extra_precision = self.extra_precision
            other = 0
        else:
            try:
                other = force_decimal(other)
            except ValueError:
                currency = Currency[other]
                other = 0
        if self.currency.rate is None or currency.rate is None:
            raise CurrencyMismatchError(
                    f"Cannot convert {self.currency} to {currency}: unknown conversion rate.")
        amount = float(self.amount) / self.currency.rate * currency.rate + float(other)
        return Money(amount, currency, extra_precision=extra_precision)

    def __irshift__(self, other):
        """In-place currency conversion.

        For instance:
            a = Money(10, 'EUR')
            a >>= 'USD'

        Will convert 10 EUR to USD.

        Can also be used to do cross-currency sums:
            a = Money(10, 'EUR')
            a >>= Money(5, 'USD')

        Will first convert 10 EUR to USD, then add 5 USD to the result and
        return the total in USD.

        This requires both currencies rate to be defined.
        """
        currency = self.currency
        extra_precision = self.extra_precision
        if isinstance(other, Money):
            currency = other.currency
            extra_precision = max(extra_precision, other.extra_precision)
            other = other.amount
        elif isinstance(other, Currency):
            currency = other
            extra_precision = self.extra_precision
            other = 0
        else:
            try:
                other = force_decimal(other)
            except ValueError:
                currency = Currency[other]
                other = 0
        if self.currency.rate is None or currency.rate is None:
            raise ValueError(f"Cannot convert {self.currency} to {currency}: "
                             "unknown conversion rate.")
        amount = float(self.amount) / self.currency.rate * currency.rate + float(other)
        return Money(amount, currency, extra_precision=extra_precision)

    def __lshift__(self, other):
        """Cross-currency sum.

        For instance:
            Money(5, 'EUR') << Money(6, 'USD')
        Will first convert 6 USD to EUR, then add 5 EUR to the result,
        to finally return the total in EUR.

        This requires both currencies rate to be defined.
        """
        if isinstance(other, Money):
            extra_precision = max(self.extra_precision, other.extra_precision)
            if self.currency == other.currency:
                other = other.amount
            else:
                other = (other >> self.currency).amount
        else:
            extra_precision = self.extra_precision
            try:
                other = force_decimal(other)
            except ValueError:
                return NotImplemented
        amount = self.amount + other
        return Money(amount, self.currency, extra_precision=extra_precision)

    def __ilshift__(self, other):
        """In-place cross-currency sum.

        For instance:
            a = Money(5, 'EUR')
            a <<= Money(6, 'USD')

        Will first convert 6 USD to EUR, then add 5 EUR to the result,
        to finally return the total in EUR.

        This requires both currencies rate to be defined.
        """
        if isinstance(other, Money):
            extra_precision = max(self.extra_precision, other.extra_precision)
            if self.currency == other.currency:
                other = other.amount
            else:
                other = (other >> self.currency).amount
        else:
            extra_precision = self.extra_precision
            try:
                other = force_decimal(other)
            except ValueError:
                return NotImplemented
        amount = self.amount + other
        return Money(amount, self.currency, extra_precision=extra_precision)

    def __abs__(self):
        return Money(abs(self.amount), self.currency, **self._options)

    def __round__(self, ndigits=None):
        return Money(round(self.amount, ndigits), self.currency, **self._options)

    def __trunc__(self):
        return Money(math.trunc(self.amount), self.currency, **self._options)

    def __floor__(self):
        return Money(math.floor(self.amount), self.currency, **self._options)

    def __ceil__(self):
        return Money(math.ceil(self.amount), self.currency, **self._options)

    def __bool__(self):
        return bool(self.amount)

    def __float__(self):
        return float(self.amount)

    def __int__(self):
        return int(self.as_scu())

    def __hash__(self):
        return hash((self.amount, self.currency))

    def __eq__(self, other):
        try:
            return self.amount == self._get_comparable_amount(other)
        except CurrencyMismatchError:
            raise
        except ValueError:
            return NotImplemented

    def __lt__(self, other):
        try:
            return self.amount < self._get_comparable_amount(other)
        except CurrencyMismatchError:
            raise
        except ValueError:
            return NotImplemented

    def __gt__(self, other):
        try:
            return self.amount > self._get_comparable_amount(other)
        except CurrencyMismatchError:
            raise
        except ValueError:
            return NotImplemented

    def __le__(self, other):
        aux = self.__gt__(other)
        return NotImplemented if aux is NotImplemented else not aux

    def __ge__(self, other):
        aux = self.__lt__(other)
        return NotImplemented if aux is NotImplemented else not aux

    @property
    def _options(self):
        return {k: getattr(self, k) for k in ['extra_precision']}

    def _get_format(self, key=None):
        if key:
            key = f'MONEY_{key}_FORMAT'
            fmt = get_format(key)
        if not key or fmt == key:
            if self.amount >= 0:
                fmt = '{code} {amount}'
            else:
                fmt = '{code} -{amount}'
        return fmt

    def _get_comparable_amount(self, other):
        if isinstance(other, Money):
            if self.currency == other.currency:
                other = other.amount
            else:
                other = (other >> self.currency).amount
        else:
            other = force_decimal(other)
        return other


def force_decimal(value, decimal_places=None):
    if not isinstance(value, Decimal):
        try:
            value = Decimal(str(value))
        except decimal.InvalidOperation:
            raise ValueError(f"\"{value}\" is not a valid decimal value.")
    if decimal_places is not None:
        value = round(value, decimal_places)
    return value
