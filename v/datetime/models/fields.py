# -*- coding: utf-8 -*-
from datetime import tzinfo

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _

import pytz

__all__ = [
    'TimezoneField',
]


@deconstructible
class TimezoneField(models.CharField):

    description = "A timezone"

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 50)
        self._default_kwargs = set()
        if 'max_length' not in kwargs:
            kwargs['max_length'] = 50
            self._default_kwargs.add('max_length')
        if 'choices' not in kwargs:
            kwargs['choices'] = [(x, x) for x in pytz.all_timezones]
            self._default_kwargs.add('choices')
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        for k in self._default_kwargs:
            del kwargs[k]
        return name, path, args, kwargs

    def from_db_value(self, value, expression, connection):
        if value is not None:
            value = pytz.timezone(value)
        return value

    def to_python(self, value):
        if value == '':
            value = None
        if value is not None and not isinstance(value, tzinfo):
            try:
                value = pytz.timezone(value)
            except pytz.UnknownTimezoneError:
                raise ValidationError(_(f"Unknown timezone: {value}"))
        return value

    def get_prep_value(self, value):
        if value == '':
            value = None
        if isinstance(value, tzinfo):
            value = str(value)
        return value
