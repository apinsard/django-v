# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import gettext_lazy as _

from .behaviors import AbstractBaseLocation

__all__ = [
    'Country', 'Division', 'Subdivision', 'City', 'District',
]


class Country(AbstractBaseLocation):
    """A country."""

    class Meta:
        verbose_name = _("country")
        verbose_name_plural = _("country")

    def get_timezone(self):
        return self.timezone


class Division(AbstractBaseLocation):
    """First-level division of a country."""

    class Meta:
        verbose_name = _("country 1st level division")
        verbose_name_plural = _("country 1st level divisions")

    country = models.ForeignKey(
        'Country', models.CASCADE, verbose_name=_("country"))

    def __str__(self):
        return "{} ({})".format(self.name, self.country.short_name)

    def get_timezone(self):
        return self.timezone or self.country.get_timezone()


class Subdivision(AbstractBaseLocation):
    """Second-level division of a country."""

    class Meta:
        verbose_name = _("country 2nd level division")
        verbose_name_plural = _("country 2nd level divisions")

    division = models.ForeignKey(
        'Area', models.CASCADE, verbose_name=_("division"))

    @property
    def country(self):
        return self.division.country

    def __str__(self):
        return "{} ({})".format(self.name, self.division.short_name)

    def get_timezone(self):
        return self.timezone or self.division.get_timezone()


class City(AbstractBaseLocation):
    """A city."""

    class Meta:
        verbose_name = _("city")
        verbose_name_plural = _("cities")

    subdivision = models.ForeignKey(
        'Subdivision', models.CASCADE, verbose_name=_("subdivision"))
    postal_codes = ArrayField(
        models.CharField(max_length=20), blank=True, null=True,
        verbose_name=_("postal codes"))

    @property
    def division(self):
        return self.subdivision.division

    @property
    def country(self):
        return self.division.country

    def __str__(self):
        return "{} ({})".format(self.name, self.subdivision.short_name)

    def get_timezone(self):
        return self.timezone or self.subdivision.get_timezone()


class District(AbstractBaseLocation):
    """Subdivision of a city."""

    class Meta:
        verbose_name = _("district")
        verbose_name_plural = _("districts")

    city = models.ForeignKey(
        'City', models.CASCADE, verbose_name=_("city"))

    @property
    def subdivision(self):
        return self.city.subdivision

    @property
    def division(self):
        return self.subdivision.division

    @property
    def country(self):
        return self.division.country

    def __str__(self):
        return "{} - {}".format(self.city, self.name)

    def get_timezone(self):
        return self.timezone or self.city.get_timezone()
