# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from v.datetime.models.fields import TimezoneField

__all__ = [
    'AbstractBaseLocation',
    'AbstractBaseLocatedEntity',
    'AbstractBaseExactLocatedEntity',
]


class AbstractBaseLocation(models.Model):
    """Any kind of broad location."""

    class Meta:
        abstract = True

    name = models.CharField(
        max_length=100,
        verbose_name=_("name"))
    slug = models.SlugField(
        max_length=100, unique=True,
        verbose_name=_("slug"))
    short_name = models.CharField(
        max_length=20, blank=True,
        verbose_name=_("short name"))
    geo_shape = models.MultiPolygonField(
        verbose_name=_("area"))
    timezone = TimezoneField(
        blank=True, null=True,
        verbose_name=_("timezone"))

    def __str__(self):
        return self.name


class AbstractBaseLocatedEntity(models.Model):
    """An entity located within a given area."""

    class Meta:
        abstract = True

    city = models.ForeignKey(
        'geo.City', models.PROTECT,
        blank=True, null=True,
        verbose_name=_("city"))
    district = models.ForeignKey(
        'geo.District', models.SET_NULL,
        blank=True, null=True,
        verbose_name=_("district"))
    postal_code = models.CharField(
        max_length=20, blank=True,
        verbose_name=_("postal code"))

    @property
    def location(self):
        """Return the most accurate location.
        Return None if no city is provided.
        """
        if self.district:
            return self.district
        else:
            return self.city

    def clean(self):
        """Force the city if the district is specified."""
        super().clean()
        if self.district:
            self.city = self.district.city

    def get_location_name(self):
        """Return the city name along with the district name if provided."""
        if not self.city:
            return "-"
        name = self.city.name
        if self.district:
            name += ", " + self.district.name
        return name


class AbstractBaseExactLocatedEntity(AbstractBaseLocatedEntity):
    """An entity located at a given point."""

    class Meta:
        abstract = True

    geo_point = models.PointField(
        blank=True, null=True,
        verbose_name=_("map position"))
    address = models.TextField(
        blank=True,
        verbose_name=_("address"))

    @property
    def latlng(self):
        if self.geo_point is None:
            return None
        return (self.geo_point.y, self.geo_point.x)
