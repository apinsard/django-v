# -*- coding: utf-8 -*-
from datetime import date

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.postgres.fields import DateRangeField
from django.core.validators import MinValueValidator
from django.db.models.enums import TextChoices
from django.utils import timezone
from djanog.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

from v.utils.models.enums import ColoredTextChoices, IconedColoredTextChoices

import pytz

__all__ = [
    'Country', 'City', 'Home', 'Reservation', 'Stay',
]


class Country(models.Model):

    name = models.CharField(
        verbose_name=_("name"), max_length=100)
    alias = models.SlugField(
        verbose_name=_("alias"), max_length=100, unique=True)
    short_name = models.CharField(
        verbose_name=_("short name"), max_length=20, blank=True)
    geo_shape = models.MultiPolygonField(
        verbose_name=_("area"))
    tz_name = models.CharField(
        verbose_name=("timezone"), blank=True, null=True)

    @cached_property
    def timezone(self):
        return pytz.timezone(self.get_tz_name())

    def get_tz_name(self):
        return self.tz_name or settings.TIME_ZONE


class City(models.Model):

    country = models.ForeignKey(
        Country, models.PROTECT, related_name='cities', verbose_name=_("country"))
    name = models.CharField(
        verbose_name=_("name"), max_length=100)
    alias = models.SlugField(
        verbose_name=_("alias"), max_length=100, unique=True)
    short_name = models.CharField(
        verbose_name=_("short name"), max_length=20, blank=True)
    geo_shape = models.MultiPolygonField(
        verbose_name=_("area"))
    tz_name = models.CharField(
        verbose_name=("timezone"), blank=True, null=True)

    @cached_property
    def timezone(self):
        return pytz.timezone(self.get_tz_name())

    def get_tz_name(self):
        return self.tz_name or self.country.get_tz_name()


class Home(models.Model):

    class State(ColoredTextChoices):
        INACTIVE = 'inactive', _("Inactive"), '#ff0000'
        HIDDEN = 'hidden', _("Invisible"), '#aaaaaa'
        ACTIVE = 'active', _("Active"), '#00ff00'

    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.PROTECT, related_name='homes',
        verbose_name=_("owner"))
    city = models.ForeignKey(City, models.PROTECT, verbose_name=_("city"))
    geo_point = models.PointField(verbose_name=_("map position"), blank=True, null=True)
    address = models.TextField(verbose_name=_("address"), blank=True)
    name = models.CharField(max_length=50, blank=True, verbose_name=_("name"))
    slug = models.SlugField(max_length=50)
    created_on = models.DateField(verbose_name=_("created on"), default=date.today)
    state = models.CharField(
        verbose_name=_("state"), choices=State.choices, default=State.INACTIVE)
    capacity = models.PositiveSmallIntegerField(
        verbose_name=_("capacity"), default=2, validators=[
            MinValueValidator(1, _("Capacity must be at least %(limit_value)s."))
        ],
        help_text=_("Number of guests that can stay in this home."))
    size = models.PositiveSmallIntegerField(
        verbose_name=_("size"), help_text=_("in squared meters"))


class Reservation(models.Model):

    class State(IconedColoredTextChoices):
        REQUEST = 'request', _("Request"), 'fas fa-question', '#0000ff'
        BOOKED = 'booked', _("Booked"), 'fas fa-check', '#00ff00'
        CANCELED = 'canceled', _("Canceled"), 'fas fa-times', '#ff0000'

    customer = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.PROTECT, related_name='reservations',
        verbose_name=_("customer"))
    reservation_date = models.DateTimeField(
        verbose_name=_("reservation date"), default=timezone.now)
    state = models.CharField(
        verbose_name=_("state"), choices=State.choices, default=State.BOOKED)
    total_price = models.MoneyField(
        verbose_name=_("total price"), editable=False, null=True)


class Stay(models.Model):

    reservation = models.ForeignKey(
        Reservation, models.PROTECT, related_name='stays', verbose_name=_("reservation"))
    home = models.ForeignKey(
        Home, models.PROTECT, related_name='stays', verbose_name=_("home"))
    guest = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.PROTECT, related_name='stays',
        verbose_name=_("guest"))
    period = DateRangeField(
        blank=True, null=True, verbose_name=_("period"))
    nb_guests = models.PositiveSmallIntegerField(
        verbose_name=_("number of guests"), default=1)
    total_price = models.MoneyField(
        verbose_name=_("total price"), editable=False, null=True)


class StayPriceLine(models.Model):

    class Category(TextChoices):
        RENT = 'rent', _("Rent")
        CLEANING = 'cleaning', _("Cleaning")
        TOURIST_TAX = 'tourist_tax', _("Tourist tax")
        EXTRA = 'extra', _("Extra services")
        SERVICE = 'service', _("Service fees")
        VAT = 'vat', _("VAT")

    stay = models.ForeignKey(
        Stay, models.CASCADE, related_name='price_lines')
    category = models.CharField(
        max_length=30, choices=Category.choices,
        verbose_name=_("category"))
    price = models.MoneyField(
        verbose_name=_("price"))
